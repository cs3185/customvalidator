package valid;

import org.mule.api.MuleEvent;
import org.mule.extension.validation.api.ValidationResult;
import org.mule.extension.validation.api.Validator;
import org.mule.extension.validation.internal.ImmutableValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author cs3185
 * 
 * Custom validator for the ValData class, will be called by Mule to validate the
 * instances of ValData as extracted from the CSV file.
 *
 */
public class ValidateValData implements Validator {
	private static Logger logger = LoggerFactory.getLogger(ValidateValData.class);

	/**
	 * Custom validator for Mule.  Only one failed field can be reported per record.
	 */
	@Override
	public ValidationResult validate(MuleEvent event) {

		Object obj = event.getMessage().getPayload();

		// Verify this is even the correct class of data 
		if (!(obj instanceof ValData)) {
			logger.warn("Validation did not find a valid instance of ValData");
			return ImmutableValidationResult.error("data received is not an instance of ValData.");
		}

		ValData data = (ValData) obj;

		// Validate the name.  It is required, and must be at least 5 and no more than 45 chars.
		if (data.getName() == null || data.getName().isEmpty()) {
			logger.warn("Required Field (name) not found.");
			return ImmutableValidationResult.error("Required Field (name) not found.");
		}
		if (data.getName().length() < 5) {
			logger.warn("Name is too short: [{}]", data.getName());
			return ImmutableValidationResult
					.error("Name [" + data.getName() + "] is too short.  Minumum 5 chars required.");
		}
		if (data.getName().length() > 45) {
			logger.warn("Name is too long: [{}]", data.getName());
			return ImmutableValidationResult
					.error("Name [" + data.getName() + "] is too long.  Minumum 45 chars accepted.");
		}

		// Validate the ID.  It is required, and must be either 1-14 digits, or
		// 1-12 digits followed by "_A"
		if (data.getID() == null || data.getID().isEmpty()) {
			logger.warn("Required field ID not found.");
			return ImmutableValidationResult.error("Required Field (number) not found.");
		}
		if (!data.getID().matches("\\d{1,14}$")) {
			if (!data.getID().matches("\\d{1,12}_A$")) {
				logger.warn("ID is not correctly formated: [{}]", data.getID());
				return ImmutableValidationResult.error(
						"Number [" + data.getID() + "] must be 1-14 digits, or 1-12 digits with an _A after only.");
			}
		}

		// Validate the Value, it is required (This field cannot actually make it past the DW
		// as null, but the validation is still here.), and must be at least 10.0 and cannot be 
		// 1000.0 or larger.
		if (data.getValue() == null) {
			logger.warn("Required field value not found");
			return ImmutableValidationResult.error("Required Field (value) not found.");
		}
		if (data.getValue() < 10.0) {
			logger.warn("Value is too small: [{}]", data.getValue());
			return ImmutableValidationResult
					.error("Value [" + data.getValue() + "] is too small, must be at least 10.0");
		}
		if (data.getValue() >= 1000.0) {
			logger.warn("Value is too large: [{}]", data.getValue());
			return ImmutableValidationResult
					.error("Value [" + data.getValue() + "] is too Large, must be less than 1000.0");
		}

		// All validations passed.
		return ImmutableValidationResult.ok();
	}
}
