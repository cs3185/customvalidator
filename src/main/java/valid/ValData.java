package valid;

import java.io.Serializable;

/**
 * @author cs3185
 *
 * Simple pojo which will be the target of the Mule DW CSV data.
 * 
 * This contains 3 fields:
 * Name: String 	Will only be considered valid if it is between
 * 					5 and 45 chars long.
 * ID: 	 String 	Will only be considered valid if between 1 and 14 chars long,
 * 					with all chars digits, or 3 to 14 chars long with the last two 
 * 					chars being "_A".
 * Value: Float		Will be valid only in [10.0..1000.0) range.
 * 
 */
public class ValData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4925219372318402026L;
	private String Name;
	private String ID;
	private Float value;
	
	public ValData() {}

	public ValData(String name, String id, Float value) {
		super();
		Name = name;
		ID = id;
		this.value = value;
	}

	public ValData(String[] fields, Float val) {
		super();
		Name = fields[0];
		ID = fields[1];
		this.value = val;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getID() {
		return ID;
	}

	public void setID(String number) {
		ID = number;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	/**
	 * Make the toString() function safe to protect private data.
	 */
	@Override
	public String toString() {
		String safeName = this.Name;
		String safeID = this.ID;
		String safeValue = String.format("%f", this.value);
		
		if (safeName != null && ! safeName.isEmpty() && safeName.length() > 5) {
			safeName = safeName.substring(0,1) + "***" + safeName.substring(safeName.length()-4);
		}
		
		if (safeID != null && ! safeID.isEmpty() && safeID.length() > 5) {
			safeID = safeID.substring(0,1) + "***" + safeID.substring(safeID.length()-4);
		}
		
		if (safeValue != null && ! safeValue.isEmpty() && safeValue.length() > 3) {
			safeValue = safeValue.substring(0,1) + "***" + safeValue.substring(safeValue.length()-1);
		}
		
		return "ValData: [Name=" + safeName + ", ID=" + safeID + ", value=" + safeValue + "]";
	}
}
